import React from 'react'
import { Route, Navigate } from 'react-router-dom'
import {auth} from '../config/firebaseconfig'

export function PrivateRoute({component: Component, ...rest}){
    const { currentUser } = auth()
    
    return(
        <div>
            <Route {...rest}
                render = {props => {
                    return  currentUser? <Component {...props}/> : <Navigate to = '/'/>
                }}>
                
            </Route>
        </div>
    )
}