import React from 'react'

export const SidebarData = [
    {
                title:'Clientes',
                path:'/clientes',
                cName: 'nav-text'
            },{
                title:'Romaneios',
                path:'/romaneios',
                cName: 'nav-text'
            },{
                title:'Movimentações',
                path:'/movimentacoes',
                cName: 'nav-text'
            },{
                title:'Contrato',
                path:'/contratos',
                cName: 'nav-text'
            },{
                title:'Conta',
                path:'/conta',
                cName: 'nav-text'
            }
]