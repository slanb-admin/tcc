import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import { AuthProvider } from '../src/context/AuthContext';
import { PrivateRoute } from './components/PrivateRoute';


import Login from '../src/pages/login'
import Admin from '../src/pages/admin'

function App() {

  
  return (
    <AuthProvider>
      <Container className='d-flex align-items-center justify-content-center'
      style={{minHeight: "100vh"}}>
        <div className='w-100' style ={{maxHeight:"400px"}}>
      <Router>
        <Routes>
        <Route path ='/login' element = {<Login/>}/>
        <Route path ='/' element = {<Admin/>}/>
        </Routes>
      </Router>
      </div>
      </Container>
      </AuthProvider>
    
    
  );
}

export default App;
