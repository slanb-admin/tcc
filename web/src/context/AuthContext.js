import React, { useContext, useEffect, useState } from 'react'
import {auth} from '../config/firebaseconfig'
import { signInWithEmailAndPassword } from "firebase/auth";

const AuthContext = React.createContext()

// function signin(email,password){
//     auth.createUserWithEmailAndPassword(email,password)
// }

export function useAuth(){
    return useContext(AuthContext)
}

export function AuthProvider( { children }){
    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
        setCurrentUser(user)
        setLoading(false)
    }, [])

    return unsubscribe
    })

    function login(email,password){
    return signInWithEmailAndPassword(auth, email, password)
    }

    const value = {
        currentUser,
        login
    }


    return(
        <AuthContext.Provider value ={value} >
            {!loading && children}
        </AuthContext.Provider>
    )
}