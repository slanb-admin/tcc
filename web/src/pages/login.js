import React, { useState, useRef } from 'react'
import {createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    }
    from 'firebase/auth'
import '../styles/login.css'
import {auth} from '../config/firebaseconfig.js'
import {Link, useNavigate} from 'react-router-dom'
import { Form, Button, Card, Alert } from 'react-bootstrap'
import {useAuth} from '../context/AuthContext'

// signOut
const Login = () => {
 // Erro ao chamar o setLoginEmail -> procurar sobre o useState
    // const [registerEmail, setRegisterEmail] = useState("")
    // const [registerPassword, setRegisterPassword] = useState("")
    const emailRef = useRef()
    const passwordRef = useRef()
    const {login}  = useAuth()
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)

    const [loginEmail, setLoginEmail] = useState("")
    const [loginPassword, setLoginPassword] = useState("")

    let navigate = useNavigate()

    // const register = async () => {
    
    //     try {
    //         const user = await createUserWithEmailAndPassword(
    //             auth,
    //             loginEmail,
    //             loginPassword)
    
    //             /*<button type='submit' onClick={register}/>*/ 
    //             console.log("Entrou aqui" + user)
    //             navigate('/')
    //     } catch (error) {
    //         console.log(error.message)
    //     }
    // }
    
    // const login = async () => {
    //     try {
    //         const user = await signInWithEmailAndPassword(
    //         auth,
    //         loginEmail,
    //         loginPassword
    //         )
    //         console.log("Entrou aqui 2" + user)
    //         navigate('/admin')

    //     } catch (error) {
    //         console.log(error.message)
    //     }
    // }
    
    //  const logout = async () => {
    //     try{
            
    //     }catch(error){
    //         console.log("Mamaram")
    //     }
    //     await signOut(auth)
    //         navigate('/')
    //     /*<button type='logout' onClick={register}/>*/
    //      /*{user?.email}*/
    //  }

async function handleSubmit(e){
        e.preventDefault()

        if (passwordRef.current.value !== passwordRef.current.value){
           return setError("Password do not match!!") 
        }
        
        try {
            setError('')
            setLoading(true)
            await login(emailRef.current.value, passwordRef.current.value)
            navigate('/')
        } catch (error) {
            setError("Failed to log in!!")
        }

        setLoading(false)
    }

    return (
        <div>
            <Card>
                <Card.Body>
                    <h2 className='text-center mb-4'>Login</h2>
                    {error && <Alert variant='danger'>{error}</Alert>}
                    <Form onSubmit={handleSubmit}>
                        <Form.Group id = "email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type = "email" ref = {emailRef} required ></Form.Control>
                        </Form.Group>

                        <Form.Group id = "password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type = "password" ref = {passwordRef} required ></Form.Control>
                        </Form.Group>

                        <Button disable = {loading} className = "w-100" type = "submit">Log In</Button>
                    </Form>
                </Card.Body>
            </Card>
        </div>
        
    )
}

export default Login