// //import React, { useState } from 'react'
// /*import {createUserWithEmailAndPassword,
//     signInWithEmailAndPassword,
//     }
//     from 'firebase/auth'*/
// import '../styles/login.css'
// import { Form, Button, Card } from 'react-bootstrap'
// //import {auth} from '../config/firebaseconfig.js'

// function Escolhas(){
//     return(
        
//     <div className='opcoes'>

// <div className='login-right'>
//     <h1>Opções</h1>
//     <button > Novo Cliente</button>

//     <button >
//         Mostrar romaneio
//     </button>

//     <button >
//         Movimentações
//     </button>

//     <button >
//         Contrato
//     </button>

//     <button type='submit'>
//         Conta
//     </button>
// </div>
// </div>
// )
// }

// export default Escolhas

// import React from 'react'

// export const Sidebars = [
//     {
//         title:'Cliente',
//         path:'/cliente',
//         cName: 'nav-text'
//     },{
//         title:'Romaneio',
//         path:'/cliente',
//         cName: 'nav-text'
//     },{
//         title:'Movimentações',
//         path:'/cliente',
//         cName: 'nav-text'
//     },{
//         title:'Contrato',
//         path:'/cliente',
//         cName: 'nav-text'
//     },{
//         title:'Conta',
//         path:'/cliente',
//         cName: 'nav-text'
//     }
// ]

import React from 'react'
import {Link} from 'react-router-dom'
import {SidebarData} from '../components/Sidebar'

function Navbar(){
    return(
        <>
        <nav className='sidebar'>
            <ul className='nav-menu-items'>
                <li className='navbar-toogle'>
                    <Link to = "#" className='menu-bars'>

                    </Link>
                </li>
                {SidebarData.map((item,index) =>{
                    return(
                        <li key={index} className={item.cName}>
                        <Link to = {item.path}>
                            <span>{item.title}</span>
                        </Link>
                        </li>
                    )
                })}
            </ul>
        </nav>
        </>
    )
}

export default Navbar