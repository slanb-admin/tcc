import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyB3K5nlaVo45u3sgb7Io9u858jhZVReBxU",
    authDomain: "tcc1-43313.firebaseapp.com",
    projectId: "tcc1-43313",
    storageBucket: "tcc1-43313.appspot.com",
    messagingSenderId: "613162784733",
    appId: "1:613162784733:web:765d2b9df51b1eb0938834",
    measurementId: "G-BL1FTLLGN8"
  };

  const app = initializeApp(firebaseConfig); 
  export const auth = getAuth(app)